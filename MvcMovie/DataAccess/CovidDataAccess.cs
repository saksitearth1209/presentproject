﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcMovie.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MvcMovie.DataAccess
{
    public class CovidDataAccess
    {
        private CovidViewModel covid;
        private List<CovidViewModel> covidList ;
     
        public async Task<CovidViewModel> CovidService()
        {

           HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

             
            HttpClient client = new HttpClient(handler);

         
            try
            {
                HttpResponseMessage response = await client.GetAsync("https://covid19.th-stat.com/api/open/today");

                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var myJObject = JObject.Parse(responseBody);
             
                //CovidViewModel covid = new CovidViewModel();
                covid = JsonConvert.DeserializeObject<CovidViewModel>(myJObject.ToString());
          
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }

            // Need to call dispose on the HttpClient and HttpClientHandler objects
            // when done using them, so the app doesn't leak resources
            handler.Dispose();
            client.Dispose();
            return covid;
        }

        public async Task <List<CovidViewModel>> CovidServiceLog()
        {

            // Create an HttpClientHandler object and set to use default credentials
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            // Create an HttpClient object
            HttpClient client = new HttpClient(handler);

            // Call asynchronous network methods in a try/catch block to handle exceptions
            try
            {
                HttpResponseMessage response = await client.GetAsync("https://covid19.th-stat.com/api/open/timeline");

                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseBody);
                JObject jsonarr = JObject.Parse(responseBody);
                
                
                covidList = JsonConvert.DeserializeObject<List<CovidViewModel>>(jsonarr.GetValue("Data").ToString());
}
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }

            handler.Dispose();
            client.Dispose();
            return covidList;
        }

    }

}



