﻿using System;
namespace MvcMovie.ViewModel
{
    public class CovidViewModel
    {
        public int Confirmed {
            get;
            set;
        }
        public int Recovered {
            get;
            set;
        }

        public int Hospitalized {
            get;
            set;
        }
        public int Deaths {
            get;
            set;
        }
        public int NewConfirmed
        {
            get;
            set;
        }
        public int NewRecovered
        {
            get;
            set;
        }
        public int NewHospitalized
        {
            get;
            set;
        }

        public int NewDeaths
        {
            get;
            set;
        }
       
        public DateTime Date
        {
            get;
            set;
        }
    }
}
