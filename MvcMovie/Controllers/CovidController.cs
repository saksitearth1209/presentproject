﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcMovie.DataAccess;
using MvcMovie.ViewModel;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MvcMovie.Controllers
{
    public class CovidController : Controller
    {
        CovidDataAccess CovidDataAccess = new CovidDataAccess();
        // GET: /<controller>/

        public async Task<IActionResult> Index()
        {
            
            CovidViewModel covid = await CovidDataAccess.CovidService();
            List<CovidViewModel> covidList = await CovidDataAccess.CovidServiceLog();
           ViewData["covid"] = covid;
            ViewData["covidList"] = covidList;
            return View();
        }

    }
}
